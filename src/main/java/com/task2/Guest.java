package com.task2;

public class Guest {

    private boolean heard;

    public Guest(boolean heard) {
        this.heard = heard;
    }

    public Guest() {

    }

    public boolean isHeard() {
        return heard;
    }

    public void setHeard(boolean heard) {
        this.heard = heard;
    }
}
