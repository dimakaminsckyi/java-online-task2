package com.task2;

/**
 * this class provide base logic of the project
 *
 * @author Dmytro Kaminsckyi
 */

import java.util.Arrays;
import java.util.Random;

public class Party {

    /**
     *  Number guests of the party
     */
    private final int numberGuests;
    private final int attempts;

    /**
     *  count amount guests who heard the rumor
     */
    private double amountGuestWhoHeard;

    /**
     * count amount full spread rumor
     */
    private double fullSpreadRumor;


    Party(int numberGuests, int attempts) {
        this.numberGuests = numberGuests;
        this.attempts = attempts;
    }

    /**
     * Create Bob and fill array Guest objects with correct size of array
     * @return full array with Guest objects
     */
    private Guest[] fillGuestArray() {
        Guest[] guestsArray = new Guest[numberGuests];
        guestsArray[0] = new Guest(true);
        for (int i = 1; i < numberGuests; i++) {
            guestsArray[i] = new Guest();
        }
        return guestsArray;
    }

    private int countNextGuest() {
        Random random = new Random();
        return random.nextInt(numberGuests);
    }

    /**
     * Main logic of project
     * Bob starts a rumor about Alice by telling it to one of the other guests.
     * A person hearing this rumor for the first time will immediately tell it to one other guest,
     * chosen at random from all the people at the party except Alice and the person from whom they heard it.
     * If a person (including Bob) hears the rumor for a second time, he or she will not propagate it further.
     */
    private void tell(Guest[] guestsArray) {
        int previousGuest = 0;
        int currentGuest = 0;
        int random;
        boolean flag = false;

        while (!flag) {
            do {
                random = countNextGuest();
            } while (random == previousGuest || random == currentGuest);
            previousGuest = currentGuest;
            currentGuest = random;
            Guest flagGuest = guestsArray[random];
            flag = flagGuest.isHeard();
            guestsArray[random].setHeard(true);
        }
        amountGuestWhoHeard += (double) Arrays.stream(guestsArray).filter(Guest::isHeard).count();
        if (Arrays.stream(guestsArray).allMatch(Guest::isHeard)) {
            fullSpreadRumor++;
        }
    }

    /**
     *  collect and run methods in the project
     */
    protected void countResult() {
        Guest[] guestsArray;
        for (int i = 0; i < attempts; i++) {
            guestsArray = fillGuestArray();
            tell(guestsArray);
        }
        showResult();

    }

    /**
     * combine strings and show result
     */
    private void showResult() {
        System.out.println("Attempts = " + attempts + "\n" + "Guests number = " + numberGuests);
        System.out.println("Number of people to hear the rumor = " + amountGuestWhoHeard / attempts + "\n" +
                "Probability that everyone will hear the rumor = " + (fullSpreadRumor / attempts) * 100 + "%");
    }

}
